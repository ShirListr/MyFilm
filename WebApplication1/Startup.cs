﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;
using System.Reflection;
using Swashbuckle.AspNetCore.Swagger;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddDbContext<WebApplication1Context>(() =>         
                options.UseInMemoryDatabase(Guid.NewGuid().ToString())
                    );
            services.AddSwaggerGen(opt => {
                opt.SwaggerDoc("v1", new Info());
            }
             );

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
    `       app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.DocumentTitle(Assembly.GetExecutingAssembly().GetName().Name);

                options.ShowRequestHeaders();
                options.ShowJsonEditor();

                options.SwaggerEndpoint("/swagger/v1/swagger.json", "ProductionCallendar API V1");
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "default",
                template: "{controller=Defolt}/{action=Index}/{id?}");
            });
        }
    }
}
