﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Film
    {
        public int ID { get; set; }
        public string FilmName { get; set; }
        public int FilmYear { get; set; }
        public string FilmGenre { get; set; }
        public float FilmDurability { get; set; }
        public float FilmRate { get; set; }
        public float FilmBudget { get; set; }

    }
}
